/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import constants.ClientEnum;
import constants.SocialNetworkConstants;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import model.ModelFacebookFeed;
import model.ModelFacebookPost;
import model.twitter.Example;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.apache.log4j.Logger;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author bkoruznjak
 */
public class NetworkController {

    private static final Logger logger = Logger.getLogger("Social APP");
    private static NetworkController mNetworkController;
    private String mTwitterAccessToken;
    private OkHttpClient mClient;

    private NetworkController() {
        mClient = new OkHttpClient();
    }

    public static NetworkController getInstance() {
        if (mNetworkController == null) {
            mNetworkController = new NetworkController();
        }
        return mNetworkController;
    }

    public void handleAntenaFacebook() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SocialNetworkConstants.FACEBOOK_REQUEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SocialApiEndpointInterface feed = retrofit.create(SocialApiEndpointInterface.class);

        Response<ModelFacebookFeed> feedModel;
        try {
            feedModel = feed.getAntenaFacebookFeed().execute();
            List<ModelFacebookPost> facebookPosts = feedModel.body().getData();
            DatabaseController dbController = new DatabaseController(ClientEnum.ANTENA);
            dbController.handleFacebookFeed(facebookPosts);
        } catch (IOException iOException) {
            logger.error("Handle Facebook IOExeption Exception {}", iOException);
        }
    }

    public void handleNarodniFacebook() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SocialNetworkConstants.FACEBOOK_REQUEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SocialApiEndpointInterface feed = retrofit.create(SocialApiEndpointInterface.class);

        Response<ModelFacebookFeed> feedModel;
        try {
            feedModel = feed.getNarodniFacebookFeed().execute();
            List<ModelFacebookPost> facebookPosts = feedModel.body().getData();
            DatabaseController dbController = new DatabaseController(ClientEnum.NARODNI);
            dbController.handleFacebookFeed(facebookPosts);
        } catch (IOException iOException) {
            logger.error("Handle Facebook IOExeption Exception {}", iOException);
        }
    }

    public void handleAntenaFacebookWithoutDatabase() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SocialNetworkConstants.FACEBOOK_REQUEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SocialApiEndpointInterface feed = retrofit.create(SocialApiEndpointInterface.class);

        Response<ModelFacebookFeed> feedModel;
        try {
            feedModel = feed.getAntenaFacebookFeed().execute();
            List<ModelFacebookPost> facebookPosts = feedModel.body().getData();
            for (ModelFacebookPost post : facebookPosts) {
                System.out.println(post);
            }
        } catch (IOException iOException) {
            logger.error("Handle Facebook IOExeption Exception {}", iOException);
        }
    }

    public void handleNarodniFacebookWithoutDatabase() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SocialNetworkConstants.FACEBOOK_REQUEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        SocialApiEndpointInterface feed = retrofit.create(SocialApiEndpointInterface.class);

        Response<ModelFacebookFeed> feedModel;
        try {
            feedModel = feed.getNarodniFacebookFeed().execute();
            List<ModelFacebookPost> facebookPosts = feedModel.body().getData();
            for (ModelFacebookPost post : facebookPosts) {
                System.out.println(post);
            }
        } catch (IOException iOException) {
            logger.error("Handle Facebook IOExeption Exception {}", iOException);
        }
    }

    public void handleAntenaTwitter() {
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(SocialNetworkConstants.TWITTER_REQUEST)
                .addHeader("Authorization", "Bearer ".concat(SocialNetworkConstants.TWITTER_ACCESS_TOKEN))
                .build();
        mClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException ioe) {
                logger.error("Handle Twitter Failure encountered with call:" + call + ", IOException raised: " + ioe);
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response rspns) throws IOException {
                GsonConverterFactory gsonFactory = GsonConverterFactory.create();
                Gson gson = new GsonBuilder().create();
                Example[] twitterFeedArray = gson.fromJson(rspns.body().string(), Example[].class);
                DatabaseController dbController = new DatabaseController(ClientEnum.ANTENA);
                dbController.handleTwitterFeed(twitterFeedArray);
            }
        });
    }

    public void handleNarodniTwitter() {
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(SocialNetworkConstants.NARODNI_TWITTER_REQUEST)
                .addHeader("Authorization", "Bearer ".concat(SocialNetworkConstants.NARODNI_TWITTER_ACCESS_TOKEN))
                .build();
        mClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException ioe) {
                logger.error("Handle Twitter Failure encountered with call:" + call + ", IOException raised: " + ioe);
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response rspns) throws IOException {
                GsonConverterFactory gsonFactory = GsonConverterFactory.create();
                Gson gson = new GsonBuilder().create();
                Example[] twitterFeedArray = gson.fromJson(rspns.body().string(), Example[].class);
                DatabaseController dbController = new DatabaseController(ClientEnum.NARODNI);
                dbController.handleTwitterFeed(twitterFeedArray);
            }
        });
    }

    public void handleAntenaTwitterWithoutDatabase() {
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(SocialNetworkConstants.TWITTER_REQUEST)
                .addHeader("Authorization", "Bearer ".concat(SocialNetworkConstants.TWITTER_ACCESS_TOKEN))
                .build();
        mClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException ioe) {
                logger.error("Handle Twitter Failure encountered with call:" + call + ", IOException raised:" + ioe);
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response rspns) throws IOException {
                GsonConverterFactory gsonFactory = GsonConverterFactory.create();
                Gson gson = new GsonBuilder().create();
                Example[] twitterFeedArray = gson.fromJson(rspns.body().string(), Example[].class);
                for (Example tweet : twitterFeedArray) {
                    System.out.println(tweet);
                }
            }
        });
    }

    public void handleNarodniTwitterWithoutDatabase() {
        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(SocialNetworkConstants.NARODNI_TWITTER_REQUEST)
                .addHeader("Authorization", "Bearer ".concat(SocialNetworkConstants.NARODNI_TWITTER_ACCESS_TOKEN))
                .build();
        mClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException ioe) {
                logger.error("Handle Twitter Failure encountered with call:" + call + ", IOException raised:" + ioe);
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response rspns) throws IOException {
                GsonConverterFactory gsonFactory = GsonConverterFactory.create();
                Gson gson = new GsonBuilder().create();
                Example[] twitterFeedArray = gson.fromJson(rspns.body().string(), Example[].class);
                for (Example tweet : twitterFeedArray) {
                    System.out.println(tweet);
                }
            }
        });
    }

    public void handleInstagram() {
        ArrayList<String> paramsList = new ArrayList<>();
        //paramsList.add("access_token=".concat(SocialNetworkConstants.INSTAGRAM_ACCESS_TOKEN));
        //String encodedSignature = generateEncodedSingature("/users/self", paramsList, SocialNetworkConstants.INSTAGRAM_CLIENT_SECRET);
        paramsList.add("access_token=".concat("3963553044.d5f31bc.5063129968684eb493364b38a6c75285"));
        String encodedSignature = generateEncodedSingature("/tags/nofilter/media/recent", paramsList, "2389dac02e1849a4a6d7c009e7067a9a");
//        System.out.println("encoded sig:" + encodedSignature);
    }

    /**
     * This method is GOLD, you need to run this to get the actual token you
     * will use in all api calls the one from the application window in the dev
     * console does jack.
     *
     * @param consumerKey
     * @param consumerSecret
     */
    public void getTwitterAccessToken(String consumerKey, String consumerSecret) {
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded;charset=UTF-8");
        byte[] requestBodyByteArray = "grant_type=client_credentials".getBytes();
        RequestBody requestBody = RequestBody.create(mediaType, requestBodyByteArray);
        Request request = new Request.Builder()
                .url(SocialNetworkConstants.TWITTER_TOKEN_REQUEST_URL)
                .post(requestBody)
                .addHeader("Authorization", "Basic ".concat(base64EncodeKeys(consumerKey, consumerSecret)))
                .build();
        mClient.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException ioe) {
                System.out.println("Failure encountered:".concat(call.toString()).concat(ioe.toString()));
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response rspns) throws IOException {
                System.out.println("Twitter done");
                System.out.println(rspns.body().toString());
                System.out.println("__________________________________________");
                System.out.println(rspns.body().string());
                mTwitterAccessToken = rspns.body().string();
            }
        });
    }

    private String base64EncodeKeys(String consumerId, String consumerSecret) {
        String combined = consumerId + ":" + consumerSecret;
        byte[] byteConsumerData = combined.getBytes(StandardCharsets.US_ASCII);
        String encodedData = Base64.encode(byteConsumerData);
        String cleanedUp = encodedData.replaceAll("\\s+", "");
        return cleanedUp;
    }

    private String getSha256Hash(String stringToBeHashed) {
        String hashedString = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(stringToBeHashed.getBytes(StandardCharsets.UTF_8));
            hashedString = hash.toString();
        } catch (NoSuchAlgorithmException noSuchAlgorithmEx) {
            System.out.println("" + noSuchAlgorithmEx.toString());
        }
        return hashedString;
    }

    private String generateEncodedSingature(String endpoint, ArrayList<String> params, String secret) {
        String signature = endpoint;
        Collections.sort(params);
        for (String field : params) {
            signature = signature.concat("|").concat(field);
        }
        System.out.println("signature:".concat(signature));
        String encodedSignature = hashValue(signature);
        return encodedSignature;
    }

    private String hashValue(String message) {
        byte[] hash = toHmacSHA256(message);
        String hashHexed = toHex(hash);
        return hashHexed;
    }

    private String toHex(byte[] value) {
        String hexed = String.format("%040x", new BigInteger(1, value));
        return hexed;
    }

    private byte[] toHmacSHA256(String value) {
        byte[] hash = null;
        try {
            SecretKey secretKey = new SecretKeySpec("6dc1787668c64c939929c17683d7cb74".getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(secretKey);
            hash = mac.doFinal(value.getBytes("UTF-8"));

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return hash;
    }
}
