/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package controller;

import model.ModelFacebookFeed;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 *
 * @author bobo
 */
public interface SocialApiEndpointInterface {

    @GET("109077262461173/posts?fields=id,created_time,link,object_id,permalink_url,message,description,picture,full_picture,source&limit=20&date_format=U&access_token=1739100849694117|U6K9Ab1qxYQGbEGd4JCEElmSCjo")
    Call<ModelFacebookFeed> getAntenaFacebookFeed();

    @GET("138846612794332/posts?fields=id,created_time,link,object_id,permalink_url,message,description,picture,full_picture,source&limit=20&date_format=U&access_token=201592430325645|9d2b14f52268e3cf75b41db9f5142d9c")
    Call<ModelFacebookFeed> getNarodniFacebookFeed();

}
