/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package controller;

import constants.ClientEnum;
import job.AntenaJob;
import job.MainJob;
import job.NarodniJob;
import org.apache.log4j.Logger;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.impl.StdSchedulerFactory;

/**
 *
 * @author bkoruznjak
 */
public class MainController {

    private static final Logger logger = Logger.getLogger("Social App");
    private static final MainController INSTANCE = new MainController();
    private Scheduler mAntenaScheduler;

    private MainController() {

    }

    public void initiate(ClientEnum client) {
        try {
            mAntenaScheduler = StdSchedulerFactory.getDefaultScheduler();
            mAntenaScheduler.start();

            JobDetail job = newJob(MainJob.class)
                    .withIdentity("mainJob", "mainGroup")
                    .build();
            switch (client) {
                case NARODNI:
                    job = newJob(NarodniJob.class)
                            .withIdentity("narodniJob", "mainGroup")
                            .build();
                    break;
                case ANTENA:
                    job = newJob(AntenaJob.class)
                            .withIdentity("antenaJob", "mainGroup")
                            .build();
                default:
                    job = newJob(MainJob.class)
                            .withIdentity("mainJob", "mainGroup")
                            .build();
                    break;
            }

            Trigger trigger = newTrigger()
                    .withIdentity("mainTrigger", "mainGroup")
                    .startNow()
                    .withSchedule(simpleSchedule()
                            .withIntervalInMinutes(1)
                            .repeatForever())
                    .build();
            mAntenaScheduler.scheduleJob(job, trigger);
        } catch (SchedulerException scheduleEx) {
            logger.error("Schedule Exception {}", scheduleEx);
        }
    }

    public static MainController getInstance() {
        return INSTANCE;
    }
}
