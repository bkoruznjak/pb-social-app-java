/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package controller;

import constants.ClientEnum;
import constants.SocialNetworkConstants;
import constants.SocialTypeConstants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.AtenaSocialTable;
import model.ModelFacebookPost;
import model.NarodniSocialTable;
import model.twitter.Example;
import model.twitter.Media;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.Base64Util;
import util.DateUtil;
import util.NewHibernateUtil;

/**
 *
 * @author bkoruznjak
 */
public class DatabaseController {

    ClientEnum mClient;

    private static final Logger logger = Logger.getLogger("Social APP");

    public DatabaseController(ClientEnum client) {
        this.mClient = client;
    }

    public List<AtenaSocialTable> fetchSocialTableContents() {
        List<AtenaSocialTable> socialTableContents = new ArrayList<>();
        SessionFactory factory = NewHibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            socialTableContents = session.createQuery("FROM SocialTable").list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return socialTableContents;
    }

    public void handleFacebookFeed(List<ModelFacebookPost> facebookPostsList) {
        SessionFactory factory = NewHibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            for (ModelFacebookPost facebookPostModel : facebookPostsList) {
                transaction = session.beginTransaction();
                String hql = "FROM AntenaSocialTable E WHERE E.id = :postModelId";
                if (mClient == ClientEnum.NARODNI) {
                    hql = "FROM NarodniSocialTable E WHERE E.id = :postModelId";
                }
                Query query = session.createQuery(hql);
                query.setParameter("postModelId", facebookPostModel.getId());
                List results = query.list();

                if (results.size() == 0) {
                    logger.info("_____________________F____________________");
                    logger.info(facebookPostModel.getId());
                    logger.info(facebookPostModel.getPostedAt());
                    logger.info(facebookPostModel.getMessage());
                    logger.info(facebookPostModel.getPermalinkUrl());
                    logger.info(facebookPostModel.getPicture());
                    logger.info(facebookPostModel.getFullPicture());
                    logger.info("_____________________F____________________");
                    //temp solution until i figure out how to store emojis in db
                    String encoded64Id = Base64Util.convertToBase64(facebookPostModel.getMessage());
                    long postedAtUnixFormat = new Long(facebookPostModel.getPostedAt());
                    if (mClient == ClientEnum.ANTENA) {
                        AtenaSocialTable newSocialEntry = new AtenaSocialTable(facebookPostModel.getId(), SocialTypeConstants.FACEBOOK, new Date(postedAtUnixFormat * 1000), encoded64Id, facebookPostModel.getPermalinkUrl(), facebookPostModel.getPicture(), facebookPostModel.getFullPicture());
                        //Query queryTwo = session.createQuery("SET NAMES utf8mb4");
                        //queryTwo.executeUpdate();
                        session.save(newSocialEntry);
                    } else if (mClient == ClientEnum.NARODNI) {
                        NarodniSocialTable newSocialEntry = new NarodniSocialTable(facebookPostModel.getId(), SocialTypeConstants.FACEBOOK, new Date(postedAtUnixFormat * 1000), encoded64Id, facebookPostModel.getPermalinkUrl(), facebookPostModel.getPicture(), facebookPostModel.getFullPicture());
                        //Query queryTwo = session.createQuery("SET NAMES utf8mb4");
                        //queryTwo.executeUpdate();
                        session.save(newSocialEntry);
                    }
                } else if (results.size() == 1) {
                    //temp solution until i figure out how to store emojis in db
                    String encoded64Id = Base64Util.convertToBase64(facebookPostModel.getMessage());
                    long postedAtUnixFormat = new Long(facebookPostModel.getPostedAt());
                    if (mClient == ClientEnum.ANTENA) {
                        AtenaSocialTable newSocialEntry = new AtenaSocialTable(facebookPostModel.getId(), SocialTypeConstants.FACEBOOK, new Date(postedAtUnixFormat * 1000), encoded64Id, facebookPostModel.getPermalinkUrl(), facebookPostModel.getPicture(), facebookPostModel.getFullPicture());
                        //Query queryTwo = session.createQuery("SET NAMES utf8mb4");
                        //queryTwo.executeUpdate();
                        if (!newSocialEntry.equals(results.get(0))) {
                            logger.info("replacing existing FACEBOOK entry in database with id:" + newSocialEntry.getId());
                            AtenaSocialTable updatedEntry = (AtenaSocialTable) results.get(0);
                            updatedEntry.setText(newSocialEntry.getText());
                            updatedEntry.setOnlineLink(newSocialEntry.getOnlineLink());
                            updatedEntry.setPictureFullLink(newSocialEntry.getPictureFullLink());
                            updatedEntry.setPictureThumbLink(newSocialEntry.getPictureThumbLink());
                            session.update(updatedEntry);
                        }
                    } else if (mClient == ClientEnum.NARODNI) {
                        NarodniSocialTable newSocialEntry = new NarodniSocialTable(facebookPostModel.getId(), SocialTypeConstants.FACEBOOK, new Date(postedAtUnixFormat * 1000), encoded64Id, facebookPostModel.getPermalinkUrl(), facebookPostModel.getPicture(), facebookPostModel.getFullPicture());
                        //Query queryTwo = session.createQuery("SET NAMES utf8mb4");
                        //queryTwo.executeUpdate();
                        if (!newSocialEntry.equals(results.get(0))) {
                            logger.info("replacing existing FACEBOOK entry in database with id:" + newSocialEntry.getId());
                            NarodniSocialTable updatedEntry = (NarodniSocialTable) results.get(0);
                            updatedEntry.setText(newSocialEntry.getText());
                            updatedEntry.setOnlineLink(newSocialEntry.getOnlineLink());
                            updatedEntry.setPictureFullLink(newSocialEntry.getPictureFullLink());
                            updatedEntry.setPictureThumbLink(newSocialEntry.getPictureThumbLink());
                            session.update(updatedEntry);
                        }
                    }
                }
                transaction.commit();
            }

        } catch (HibernateException hibEx) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error("Facebook Hibernate Exception: " + hibEx);
        } finally {
            session.close();
        }
    }

    public void handleTwitterFeed(Example[] twitterFeedArray) {
        SessionFactory factory = NewHibernateUtil.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = null;
        try {
            for (Example tweet : twitterFeedArray) {
                transaction = session.beginTransaction();
                String hql = "FROM SocialTable E WHERE E.id = :postModelId";
                if (mClient == ClientEnum.NARODNI) {
                    hql = "FROM NarodniSocialTable E WHERE E.id = :postModelId";
                }
                Query query = session.createQuery(hql);
                query.setParameter("postModelId", tweet.getIdStr());
                List results = query.list();

                if (results.size() == 0) {
                    logger.info("_____________________T____________________");
                    logger.info(tweet.getIdStr());
                    logger.info(tweet.getCreatedAt());
                    logger.info(tweet.getText());
                    switch (mClient) {
                        case NARODNI:
                            logger.info(SocialNetworkConstants.NARODNI_TWITTER_STATUS_BASE.concat("" + tweet.getId()));
                            break;
                        case ANTENA:
                            logger.info(SocialNetworkConstants.TWITTER_STATUS_BASE.concat("" + tweet.getId()));
                            break;
                        default:
                            logger.info(SocialNetworkConstants.TWITTER_STATUS_BASE.concat("" + tweet.getId()));
                            break;
                    }

                    List<Media> tweetUrls = tweet.getEntities().getMedia();
                    String thumbImageUrl = "null";
                    String fullImageUrl = "null";
                    if (tweetUrls.size() != 0) {
                        Media databaseMedia = tweetUrls.get(0);
                        logger.info(databaseMedia.getMediaUrl().concat(":thumb"));
                        thumbImageUrl = databaseMedia.getMediaUrl().concat(":thumb");
                        logger.info(databaseMedia.getMediaUrl().concat(":large"));
                        fullImageUrl = databaseMedia.getMediaUrl().concat(":large");
                    }
                    logger.info("_____________________T____________________");

                    //temp solution until i figure out how to store emojis in db
                    String encoded64Text = Base64Util.convertToBase64(tweet.getText());
                    Date createdAtDate = DateUtil.convertTwitterFormatToDate(tweet.getCreatedAt());

                    switch (mClient) {
                        case NARODNI:
                            NarodniSocialTable narodniSocialEntry = new NarodniSocialTable(tweet.getIdStr(), SocialTypeConstants.TWITTER, createdAtDate, encoded64Text, SocialNetworkConstants.NARODNI_TWITTER_STATUS_BASE.concat(tweet.getIdStr()), thumbImageUrl, fullImageUrl);
                            session.save(narodniSocialEntry);
                            break;
                        case ANTENA:
                            AtenaSocialTable antenaSocialEntry = new AtenaSocialTable(tweet.getIdStr(), SocialTypeConstants.TWITTER, createdAtDate, encoded64Text, SocialNetworkConstants.TWITTER_STATUS_BASE.concat(tweet.getIdStr()), thumbImageUrl, fullImageUrl);
                            session.save(antenaSocialEntry);
                            break;
                    }

                    //Query queryTwo = session.createQuery("SET NAMES utf8mb4");
                    //queryTwo.executeUpdate();
                } else if (results.size() == 1) {
                    List<Media> tweetUrls = tweet.getEntities().getMedia();
                    String thumbImageUrl = "null";
                    String fullImageUrl = "null";
                    if (tweetUrls.size() != 0) {
                        Media databaseMedia = tweetUrls.get(0);
                        thumbImageUrl = databaseMedia.getMediaUrl().concat(":thumb");
                        fullImageUrl = databaseMedia.getMediaUrl().concat(":large");
                    }

                    //temp solution until i figure out how to store emojis in db
                    String encoded64Text = Base64Util.convertToBase64(tweet.getText());
                    Date createdAtDate = DateUtil.convertTwitterFormatToDate(tweet.getCreatedAt());
                    if (mClient == ClientEnum.ANTENA) {
                        AtenaSocialTable newSocialEntry = new AtenaSocialTable(tweet.getIdStr(), SocialTypeConstants.TWITTER, createdAtDate, encoded64Text, SocialNetworkConstants.TWITTER_STATUS_BASE.concat(tweet.getIdStr()), thumbImageUrl, fullImageUrl);
                        if (!newSocialEntry.equals(results.get(0))) {
                            logger.info("replacing existing TWITTER entry in database with id:" + newSocialEntry.getId());
                            AtenaSocialTable updatedEntry = (AtenaSocialTable) results.get(0);
                            updatedEntry.setText(newSocialEntry.getText());
                            updatedEntry.setOnlineLink(newSocialEntry.getOnlineLink());
                            updatedEntry.setPictureFullLink(newSocialEntry.getPictureFullLink());
                            updatedEntry.setPictureThumbLink(newSocialEntry.getPictureThumbLink());
                            session.update(updatedEntry);
                        }
                    } else if (mClient == ClientEnum.NARODNI) {
                        NarodniSocialTable newSocialEntry = new NarodniSocialTable(tweet.getIdStr(), SocialTypeConstants.TWITTER, createdAtDate, encoded64Text, SocialNetworkConstants.TWITTER_STATUS_BASE.concat(tweet.getIdStr()), thumbImageUrl, fullImageUrl);
                        if (!newSocialEntry.equals(results.get(0))) {
                            logger.info("replacing existing TWITTER entry in database with id:" + newSocialEntry.getId());
                            NarodniSocialTable updatedEntry = (NarodniSocialTable) results.get(0);
                            updatedEntry.setText(newSocialEntry.getText());
                            updatedEntry.setOnlineLink(newSocialEntry.getOnlineLink());
                            updatedEntry.setPictureFullLink(newSocialEntry.getPictureFullLink());
                            updatedEntry.setPictureThumbLink(newSocialEntry.getPictureThumbLink());
                            session.update(updatedEntry);
                        }
                    }
                }
                transaction.commit();
            }

        } catch (HibernateException hibEx) {
            if (transaction != null) {
                transaction.rollback();
            }
            logger.error("Twitter Hibernate Exception:" + hibEx);
        } finally {
            session.close();
        }
    }
}
