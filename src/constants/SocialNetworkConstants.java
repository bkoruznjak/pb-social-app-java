/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package constants;

/**
 *
 * @author bkoruznjak
 */
public class SocialNetworkConstants {

    public static final String ANDROID_DEBUG_KEY_HASH = "ga0RGNYHvNM5d0SLGQfpQWAPGJ8=";
    public static final String FABRIC_APP_ID = "3c0c9b036e18eedcf8f080a3ca424277eaeb1c56";
    public static final String FACEBOOK_APP_ID = "1739100849694117";
    public static final String FACEBOOK_APP_SECRET = "2be3d0033ab8bdd5eb8de0a83c5bc695";
    public static final String FACEBOOK_ACCESS_TOKEN = "1739100849694117|U6K9Ab1qxYQGbEGd4JCEElmSCjo";
    public static final String FACEBOOK_REQUEST_URL = "https://graph.facebook.com/";
    public static final String FACEBOOK_FULL_REQUEST_URL = "https://graph.facebook.com/109077262461173/posts?fields=id,created_time,link,object_id,permalink_url,message,description,picture,full_picture,source&limit=1&access_token=1739100849694117|2be3d0033ab8bdd5eb8de0a83c5bc695";
    public static final String TWITTER_TOKEN_REQUEST_URL = "https://api.twitter.com/oauth2/token";
    public static final String TWITTER_CONSUMER_KEY = "dHf7bJ56xpr5BtSQz1AjHZK1F";
    public static final String TWITTER_CONSUMER_SECRET = "8LgM9kpzQjOq8wtab5JF0niPpSyasAGBUyh8eOp1CC0VrdwMyt";
    public static final String TWITTER_ACCESS_TOKEN = "AAAAAAAAAAAAAAAAAAAAACjKgAAAAAAAlcRN2LgYdD6F%2Fzk2GwV4DdVak2g%3DpcfpLW1qhkmZdFjvsk1Vzxn6vbTnGBJ9DFYpPiFFtHU874q3MP";
    public static final String TWITTER_BASE = "https://api.twitter.com/";
    public static final String TWITTER_STATUS_BASE = "https://twitter.com/AntenaZgb/status/";
    public static final String TWITTER_REQUEST = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=AntenaZGB&count=20";
    public static final String INSTAGRAM_CLIENT_ID = "d5f31bcfbeeb43b39708d6d6d790fd12";
    public static final String INSTAGRAM_CLIENT_SECRET = "2389dac02e1849a4a6d7c009e7067a9a";
    public static final String INSTAGRAM_ACCESS_TOKEN = "3963553044.d5f31bc.5063129968684eb493364b38a6c75285";
    public static final String PINTEREST_APP_ID = "4858187835401840879";
    public static final String PINTEREST_APP_SECRET = "83d1fee3897e2a6c651294e0a88249deb79c49e770d805ff59616bb79453b9f0";

    public static final String NARODNI_FACEBOOK_CLIENT_ID = "201592430325645";
    public static final String NARODNI_FACEBOOK_CLIENT_SECRET = "9d2b14f52268e3cf75b41db9f5142d9c";
    public static final String NARODNI_FACEBOOK_FULL_REQUEST_URL = "https://graph.facebook.com/138846612794332/posts?fields=id,created_time,link,object_id,permalink_url,message,description,picture,full_picture,source&limit=1&access_token=201592430325645|9d2b14f52268e3cf75b41db9f5142d9c";

    public static final String NARODNI_TWITTER_CLIENT_ID = "eksV10oVoOpEGpdAGcc6qXs5W";
    public static final String NARODNI_TWITTER_CLIENT_SECRET = "zEMkzh3dS5ReoUIlGfsLFdN3odltAeQwBbro6KSLT7EKLUelm3";
    public static final String NARODNI_TWITTER_ACCESS_TOKEN = "AAAAAAAAAAAAAAAAAAAAAA22zgAAAAAAqCuvkFoVlIkfRx6Xm48ybTVaSoI%3DYP0rjc9ehgTsgIXglX4qgf6g7l1HPL2tsE1bLrzJBYffsu2bRP";
    public static final String NARODNI_TWITTER_STATUS_BASE = "https://twitter.com/narodniradio/status/";
    public static final String NARODNI_TWITTER_REQUEST = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=narodniradio&count=20";

}
