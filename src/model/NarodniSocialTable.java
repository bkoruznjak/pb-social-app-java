/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author bkoruznjak
 */
@Entity
@Table(schema = "narodni", name = "social_table")
public class NarodniSocialTable {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "type")
    private String type;

    @Column(name = "posted_at", columnDefinition = "DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date postedAt;

    @Column(name = "text")
    private String text;

    @Column(name = "online_link")
    private String onlineLink;

    @Column(name = "picture_thumb_link")
    private String pictureThumbLink;

    @Column(name = "picture_full_link")
    private String pictureFullLink;

    public NarodniSocialTable() {
    }

    public NarodniSocialTable(String id, String type, Date postedAt, String text, String onlineLink, String pictureThumbLink, String pictureFullLink) {
        this.id = id;
        this.type = type;
        this.postedAt = postedAt;
        this.text = text;
        this.onlineLink = onlineLink;
        this.pictureThumbLink = pictureThumbLink;
        this.pictureFullLink = pictureFullLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Date postedAt) {
        this.postedAt = postedAt;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOnlineLink() {
        return onlineLink;
    }

    public void setOnlineLink(String onlineLink) {
        this.onlineLink = onlineLink;
    }

    public String getPictureThumbLink() {
        return pictureThumbLink;
    }

    public void setPictureThumbLink(String pictureThumbLink) {
        this.pictureThumbLink = pictureThumbLink;
    }

    public String getPictureFullLink() {
        return pictureFullLink;
    }

    public void setPictureFullLink(String pictureFullLink) {
        this.pictureFullLink = pictureFullLink;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof NarodniSocialTable) {
            String id1 = (this.id) != null ? this.id : "null";
            String text1 = (this.text) != null ? this.text : "null";
            String onlineLink1 = (this.onlineLink) != null ? this.onlineLink : "null";
            String pictureThumbLink1 = (this.pictureThumbLink) != null ? this.pictureThumbLink : "null";
            String pictureFullLink1 = (this.pictureFullLink) != null ? this.pictureFullLink : "null";

            String id2 = (((NarodniSocialTable) obj).getId()) != null ? ((NarodniSocialTable) obj).getId() : "null";
            String text2 = (((NarodniSocialTable) obj).getText()) != null ? ((NarodniSocialTable) obj).getText() : "null";
            String onlineLink2 = (((NarodniSocialTable) obj).getOnlineLink()) != null ? ((NarodniSocialTable) obj).getOnlineLink() : "null";
            String pictureThumbLink2 = (((NarodniSocialTable) obj).getPictureThumbLink()) != null ? ((NarodniSocialTable) obj).getPictureThumbLink() : "null";
            String pictureFullLink2 = (((NarodniSocialTable) obj).getPictureFullLink()) != null ? ((NarodniSocialTable) obj).getPictureFullLink() : "null";

            return (id1.equals(id2) && text1.equals(text2) && onlineLink1.equals(onlineLink2) && pictureThumbLink1.equals(pictureThumbLink2) && pictureFullLink1.equals(pictureFullLink2));
        } else {
            return super.equals(obj);
        }
    }

}
