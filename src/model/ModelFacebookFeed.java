/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

/**
 *
 * @author bkoruznjak
 */
@Generated("org.jsonschema2pojo")
public class ModelFacebookFeed {

    private List<ModelFacebookPost> data = new ArrayList<ModelFacebookPost>();
    private ModelFacebookPaging paging;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The data
     */
    public List<ModelFacebookPost> getData() {
        return data;
    }

    /**
     *
     * @param data The data
     */
    public void setData(List<ModelFacebookPost> data) {
        this.data = data;
    }

    /**
     *
     * @return The paging
     */
    public ModelFacebookPaging getPaging() {
        return paging;
    }

    /**
     *
     * @param paging The paging
     */
    public void setPaging(ModelFacebookPaging paging) {
        this.paging = paging;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
