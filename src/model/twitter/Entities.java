/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package model.twitter;

/**
 *
 * @author bobo
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Entities {

    private List<Hashtag> hashtags = new ArrayList<Hashtag>();
    private List<Object> symbols = new ArrayList<Object>();
    private List<Object> userMentions = new ArrayList<Object>();
    private List<Object> urls = new ArrayList<Object>();
    private List<Media> media = new ArrayList<Media>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The hashtags
     */
    public List<Hashtag> getHashtags() {
        return hashtags;
    }

    /**
     *
     * @param hashtags The hashtags
     */
    public void setHashtags(List<Hashtag> hashtags) {
        this.hashtags = hashtags;
    }

    /**
     *
     * @return The symbols
     */
    public List<Object> getSymbols() {
        return symbols;
    }

    /**
     *
     * @param symbols The symbols
     */
    public void setSymbols(List<Object> symbols) {
        this.symbols = symbols;
    }

    /**
     *
     * @return The userMentions
     */
    public List<Object> getUserMentions() {
        return userMentions;
    }

    /**
     *
     * @param userMentions The user_mentions
     */
    public void setUserMentions(List<Object> userMentions) {
        this.userMentions = userMentions;
    }

    /**
     *
     * @return The urls
     */
    public List<Object> getUrls() {
        return urls;
    }

    /**
     *
     * @param urls The urls
     */
    public void setUrls(List<Object> urls) {
        this.urls = urls;
    }
    
    public List<Media> getMedia(){
        return media;
    }
    
    public void setMedia(List<Media> media){
        this.media = media;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
