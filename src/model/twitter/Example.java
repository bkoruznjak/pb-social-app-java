/*
 * The MIT License
 *
 * Copyright 2016 bobo.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package model.twitter;

import com.google.gson.annotations.SerializedName;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;

@Generated("org.jsonschema2pojo")
public class Example {

    @SerializedName("created_at")
    private String createdAt;
    private long id;
    @SerializedName("id_str")
    private String idStr;
    private String text;
    private boolean truncated;
    private Entities entities;
    private String source;
    private Object inReplyToStatusId;
    private Object inReplyToStatusIdStr;
    private Object inReplyToUserId;
    private Object inReplyToUserIdStr;
    private Object inReplyToScreenName;
    private User user;
    private Object geo;
    private Object coordinates;
    private Object place;
    private Object contributors;
    private boolean isQuoteStatus;
    private int retweetCount;
    private int favoriteCount;
    private boolean favorited;
    private boolean retweeted;
    private String lang;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     *
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     *
     * @return The id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return The idStr
     */
    public String getIdStr() {
        return idStr;
    }

    /**
     *
     * @param idStr The id_str
     */
    public void setIdStr(String idStr) {
        this.idStr = idStr;
    }

    /**
     *
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return The truncated
     */
    public boolean isTruncated() {
        return truncated;
    }

    /**
     *
     * @param truncated The truncated
     */
    public void setTruncated(boolean truncated) {
        this.truncated = truncated;
    }

    /**
     *
     * @return The entities
     */
    public Entities getEntities() {
        return entities;
    }

    /**
     *
     * @param entities The entities
     */
    public void setEntities(Entities entities) {
        this.entities = entities;
    }

    /**
     *
     * @return The source
     */
    public String getSource() {
        return source;
    }

    /**
     *
     * @param source The source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     *
     * @return The inReplyToStatusId
     */
    public Object getInReplyToStatusId() {
        return inReplyToStatusId;
    }

    /**
     *
     * @param inReplyToStatusId The in_reply_to_status_id
     */
    public void setInReplyToStatusId(Object inReplyToStatusId) {
        this.inReplyToStatusId = inReplyToStatusId;
    }

    /**
     *
     * @return The inReplyToStatusIdStr
     */
    public Object getInReplyToStatusIdStr() {
        return inReplyToStatusIdStr;
    }

    /**
     *
     * @param inReplyToStatusIdStr The in_reply_to_status_id_str
     */
    public void setInReplyToStatusIdStr(Object inReplyToStatusIdStr) {
        this.inReplyToStatusIdStr = inReplyToStatusIdStr;
    }

    /**
     *
     * @return The inReplyToUserId
     */
    public Object getInReplyToUserId() {
        return inReplyToUserId;
    }

    /**
     *
     * @param inReplyToUserId The in_reply_to_user_id
     */
    public void setInReplyToUserId(Object inReplyToUserId) {
        this.inReplyToUserId = inReplyToUserId;
    }

    /**
     *
     * @return The inReplyToUserIdStr
     */
    public Object getInReplyToUserIdStr() {
        return inReplyToUserIdStr;
    }

    /**
     *
     * @param inReplyToUserIdStr The in_reply_to_user_id_str
     */
    public void setInReplyToUserIdStr(Object inReplyToUserIdStr) {
        this.inReplyToUserIdStr = inReplyToUserIdStr;
    }

    /**
     *
     * @return The inReplyToScreenName
     */
    public Object getInReplyToScreenName() {
        return inReplyToScreenName;
    }

    /**
     *
     * @param inReplyToScreenName The in_reply_to_screen_name
     */
    public void setInReplyToScreenName(Object inReplyToScreenName) {
        this.inReplyToScreenName = inReplyToScreenName;
    }

    /**
     *
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     *
     * @return The geo
     */
    public Object getGeo() {
        return geo;
    }

    /**
     *
     * @param geo The geo
     */
    public void setGeo(Object geo) {
        this.geo = geo;
    }

    /**
     *
     * @return The coordinates
     */
    public Object getCoordinates() {
        return coordinates;
    }

    /**
     *
     * @param coordinates The coordinates
     */
    public void setCoordinates(Object coordinates) {
        this.coordinates = coordinates;
    }

    /**
     *
     * @return The place
     */
    public Object getPlace() {
        return place;
    }

    /**
     *
     * @param place The place
     */
    public void setPlace(Object place) {
        this.place = place;
    }

    /**
     *
     * @return The contributors
     */
    public Object getContributors() {
        return contributors;
    }

    /**
     *
     * @param contributors The contributors
     */
    public void setContributors(Object contributors) {
        this.contributors = contributors;
    }

    /**
     *
     * @return The isQuoteStatus
     */
    public boolean isIsQuoteStatus() {
        return isQuoteStatus;
    }

    /**
     *
     * @param isQuoteStatus The is_quote_status
     */
    public void setIsQuoteStatus(boolean isQuoteStatus) {
        this.isQuoteStatus = isQuoteStatus;
    }

    /**
     *
     * @return The retweetCount
     */
    public int getRetweetCount() {
        return retweetCount;
    }

    /**
     *
     * @param retweetCount The retweet_count
     */
    public void setRetweetCount(int retweetCount) {
        this.retweetCount = retweetCount;
    }

    /**
     *
     * @return The favoriteCount
     */
    public int getFavoriteCount() {
        return favoriteCount;
    }

    /**
     *
     * @param favoriteCount The favorite_count
     */
    public void setFavoriteCount(int favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    /**
     *
     * @return The favorited
     */
    public boolean isFavorited() {
        return favorited;
    }

    /**
     *
     * @param favorited The favorited
     */
    public void setFavorited(boolean favorited) {
        this.favorited = favorited;
    }

    /**
     *
     * @return The retweeted
     */
    public boolean isRetweeted() {
        return retweeted;
    }

    /**
     *
     * @param retweeted The retweeted
     */
    public void setRetweeted(boolean retweeted) {
        this.retweeted = retweeted;
    }

    /**
     *
     * @return The lang
     */
    public String getLang() {
        return lang;
    }

    /**
     *
     * @param lang The lang
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "" + this.idStr
                + ", " + this.text;
    }

}
